Summary
=========

Simple serializer to serialize objects in an EOQ domain as EOQ commands and deserialize to EOQ objects in an EOQ domain them respectively. 

Notes
=========
- Not optimized for speed. Serialization is currently slow because queries are sent sequentially to the EOQ server.
- Does not serialize/deserialize meta-model information, i.e. the compatible meta-model must be available in the target EOQ domain as well. 
- Supports only EOQ version 2. 