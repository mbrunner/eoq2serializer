
# EOQ2
import base64
from eoq2.domain.local import LocalMdbDomain
from eoq2 import Get,Set,Add,Rem,Mov,Clo,Crt,Crn,Sts,Chg,Gaa,Cal,Asc,Abc,Cmp,CloModes,Obj
from eoq2 import Qry,Obj,His,Cls,Ino,Met,Idx,Pth,Arr
from eoq2.serialization import JsonSerializer,TextSerializer,PySerializer,JsSerializer
from eoq2.query.query import ObjSeg

class EOQ2Serializer():
    def __init__(self,domain,eoqObj):
        self.domain=domain
        self.commands=[]
        print('Serializing '+str(eoqObj))
        self.createdObjects={}
        print('Building commands...')
        self.findOrAdd(eoqObj)
        print('Serialization complete.')

    def findOrAdd(self,eoqObj):
        if str(eoqObj) not in self.createdObjects:
            self.createdObjects[str(eoqObj)]=self.createObject(eoqObj)
        return self.createdObjects[str(eoqObj)]
    
    def serializeValue(self,value):
        svalue=value
        if isinstance(value, list):
            svalue='['+','.join([self.serializeValue(v) for v in value])+']'
        elif isinstance(value,ObjSeg):
            svalue='$'+str(self.findOrAdd(value))
        elif isinstance(value, str):
            svalue="'"+str(value)+"'"
        elif value is None:
            svalue='%'
        else:
            svalue=str(value)  
        return svalue
    
    def encode(self,value):
        encoded=base64.b64encode(value.encode('ascii')).decode('ascii')
        return encoded   
    
    def decode(self,encoded):
        decoded=base64.b64decode(encoded.encode('ascii')).decode('ascii')
        return decoded    

    def set(self,eoqObj,attribute):        
        if str(eoqObj) not in self.createdObjects:
            raise ValueError('Cannot set attribute to unknown object '+str(eoqObj))
        objHistoryId=self.createdObjects[str(eoqObj)]
        attrval=self.domain.Do(Get(Qry(eoqObj).Pth(attribute)))
        value=self.serializeValue(attrval)
        value=self.encode(value)
        target=self.encode('$'+str(objHistoryId))
        attributename=self.encode("'"+attribute+"'")
        if not isinstance(attrval,list):
            setCommand='SET '+str(target)+" "+str(attributename)+" "+value
        else:
            setCommand='ADD '+str(target)+" "+str(attributename)+" "+value
        historyId=len(self.commands)
        self.commands.append(setCommand)
        return historyId
    
    def createObject(self,eoqObj):
        nsURI=self.encode("'"+self.domain.Do(Get(Qry(eoqObj).Met('CLASS').Met('PACKAGE').Pth('nsURI')))+"'")
        classname=self.encode("'"+self.domain.Do(Get(Qry(eoqObj).Met('CLASSNAME')))+"'")
        n=self.encode(str(1))
        crnCommand="CRN "+str(nsURI)+" "+str(classname)+" "+str(n)
        historyId=len(self.commands)
        self.commands.append(crnCommand)
        self.createdObjects[str(eoqObj)]=historyId
        attributes=self.domain.Do(Get(Qry(eoqObj).Met('ATTRIBUTENAMES')))
        for attribute in attributes:
            self.set(eoqObj,attribute)
        references=self.domain.Do(Get(Qry(eoqObj).Met('REFERENCENAMES')))
        for reference in references:
            self.set(eoqObj,reference)
        return historyId
    
    def dump(self,path):
        print('Dumping commands to '+str(path))
        f = open(path, "w")
        for command in self.commands:
            f.write(str(command)+"\n")
        f.close()
        print('Commands dumped.')


class EOQ2Deserializer():
    def __init__(self,domain,path):
        self.domain=domain
        self.commands=[]
        self.path=path
        print('Deserializing from '+str(path))
        self.load(path)

    def load(self,path):
        f = open(path, "r")
        self.commands=f.read()
        f.close()

    def decode(self,encoded):
        decoded=base64.b64decode(encoded.encode('ascii')).decode('ascii')
        return str(decoded)    
    
    def deserializeValue(self,value):
        dvalue=value
        if value == '%':
            dvalue=None
        elif value[0] == "'" and value[-1]=="'":
            # String vlaue
            dvalue=value.strip().replace("'","")
        elif value[0] == "[" and value[-1]=="]":
            # List value
            lvalue=value.strip().replace("[","").replace("]","")
            if ',' in lvalue:
                dvalue=Arr([self.deserializeValue(v) for v in lvalue.split(',')])
            else:
                if len(lvalue)>0:
                    dvalue=Arr([self.deserializeValue(lvalue)])
                else:
                    dvalue=Arr([])
        elif value[0]=="$":
            # history value
            hvalue=value.replace("$","")
            dvalue=His(int(hvalue))
        elif value=='False':
            dvalue=False
        elif value=='True':
            dvalue=True
        else:
            # assume numeric value
            if "." in value or "e" in value:
                dvalue=float(value)
            else:
                dvalue=int(value)           

        return dvalue
    
    def execute(self):      
        cmd=Cmp()
        def crndes(*args):
            nonlocal cmd
            dargs=[self.deserializeValue(self.decode(arg)) for arg in args[0]]
            cmd.Crn(dargs[0],dargs[1],dargs[2])
       
        def setdes(*args):
            nonlocal cmd
            dargs=[self.deserializeValue(self.decode(arg)) for arg in args[0]]
            cmd.Set(dargs[0],dargs[1],dargs[2])

        def adddes(*args):
            nonlocal cmd            
            dargs=[self.deserializeValue(self.decode(arg)) for arg in args[0]]
            cmd.Add(dargs[0],dargs[1],dargs[2])

        supportedCommands={'CRN':crndes, 'SET':setdes,'ADD':adddes}
        self.lcommands=self.commands.split("\n")      
     
        print('Reading commands...')
        for lcommand in self.lcommands:
            if lcommand != "":
                commandparts=lcommand.split(" ")
                if commandparts[0] in supportedCommands.keys():
                    supportedCommands[commandparts[0]](commandparts[1:])                    
                else:
                    raise ValueError('Deserialization of command '+str(commandparts[0]+' not supported'))
                 
        print('Rebuilding objects...')    
        
        res=self.domain.Do(cmd)[0]
        print('Deserialization complete')
        return res
 
        